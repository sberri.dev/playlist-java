import java.util.HashSet;
import java.util.Set;

class Music {
  private String title;
  private int duration;
  private Set<Artist> artistSet = new HashSet<>();

  // Constructor
  public Music(String title, int duration, Set<Artist> artistSet) {
    this.title = title;
    this.duration = duration;
    this.artistSet = artistSet;
  }

  public void addArtist(Artist artist) {
    this.artistSet.add(artist);
}

  // Getteur pour récupérer la durée de la chanson dans main
  public int getDuration() {
    return this.duration;
  }

  public String getInfos() {
    // Convertir la durée en minutes et secondes
    int minutes = duration / 60;
    int seconds = duration % 60; // Opération modulo pour donner le reste en seconde

    // conversiont de la durée au format mm:ss avec la méthode String.format()
    String newDuration = String.format("%02d:%02d", minutes, seconds);

    String nameOfArtist = "";
    for (Artist artist : artistSet) {
      nameOfArtist += artist.getFullName();
    }

    return this.title + " " + newDuration + " " + nameOfArtist;
  }
}
