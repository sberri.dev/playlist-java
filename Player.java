import java.util.HashSet;

public class Player {
    public static void main(String[] args) {

        Artist MichaelJackson = new Artist("Michael", "Jackson");
        Artist arethaFranklin = new Artist("Aretha", "Franklin");
        Artist ladyGaga = new Artist("Lady", "Gaga");
        System.out.println(MichaelJackson.getFullName());
        System.out.println(ladyGaga.getFullName());

        HashSet<Artist> firstMusic = new HashSet<>();
        firstMusic.add(MichaelJackson);
        HashSet<Artist> secondMusic = new HashSet<>();
        secondMusic.add(arethaFranklin);
        HashSet<Artist> thirdMusic = new HashSet<>();
        thirdMusic.add(ladyGaga);

        Music beatIt = new Music("Beat it", 305, firstMusic);
        Music respect = new Music("Respect", 255, secondMusic);
        Music pokerFace = new Music("Poker Face", 221, thirdMusic);

        Playlist bestPlaylist = new Playlist();
        bestPlaylist.add(beatIt);
        bestPlaylist.add(respect);
        bestPlaylist.add(pokerFace);

        System.out.println(beatIt.getInfos());
        System.out.println(pokerFace.getInfos());

        bestPlaylist.next();
        bestPlaylist.next();

        System.out.println(bestPlaylist.getTotalDuration());


    }
}
