public class Artist { 
   private String firstName; 
   private String lastName; 

// Constructor 
  public Artist(String firstNameValue, String lastNameValue) {
    this.firstName = firstNameValue;
    this.lastName = lastNameValue;
  }

// Method 
  public String getFullName() {
     return this.firstName + " " + this.lastName;
    }
}



