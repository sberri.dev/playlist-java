import java.util.ArrayList;
import java.util.List;

class Playlist {

    // Les attributs sont toujours Privées (private)
    private Music currentMusic; //Music ici devient un type (les classes peuvent être des types)
    private List<Music> musicList = new ArrayList<>();

    // Constructeur
    // public Playlist(Music currentMusic, List<Music> musicList) {
    //     this.currentMusic = currentMusic;
    //     this.musicList = new ArrayList<>();
    // }

    // Ajout d'une musique à musicList
    public void add(Music music) {
        musicList.add(music);
    }

    // Suppression d'une musique de la liste par rapport à son positionnement
    public void remove(int position) {
        if (position >= 0 && position < musicList.size()) {
            musicList.remove(position);
        }
    }

    // Méthode pour calculer la durée total de toutes les musiques de la playlist
    public int getTotalDuration() {
        int totalDuration = 0; 
        for (Music music : musicList) { // For (type variable : collection) 
            totalDuration += music.getDuration();
        }
        return totalDuration;
    }

    // Méthode pour passer à la musique suivante
    public void next() {

        // Cette condition avec la méthode isEmpty() permet de voir si la liste de la playlist est vide
        if (this.musicList.isEmpty()) {
            System.out.println("Musique introuvable");
        } else {
            this.currentMusic = this.musicList.get(0);
            this.musicList.remove(0); // La méthode .remove() sert à supprimer la chanson de la liste pour pouvoir passer à la chanson suivante
            System.out.println("Lecture de la musique: " + currentMusic.getInfos());
        }
    }
}